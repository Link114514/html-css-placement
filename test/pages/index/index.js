Page({
  data: {
    current: 0,
    pressingButton: null,
    buttonClicked: {
      classic1: false,
      classic2: false,
      classic3: false,
    },
  },
  onLoad: function () {
    // 设置定时器，每隔5000毫秒（5秒）切换一次图片
    setInterval(() => {
      let current = this.data.current;
      current = (current + 1) % 3; // 有3张图片
      this.setData({
        current: current,
      });
    }, 5000);
  },
  navigateToClassic: function (event) {
    const type = event.currentTarget.dataset.type;
    wx.navigateTo({
      url: `/pages/南山/南山`
    });
  },
  handleButtonClick: function (event) {
    const type = event.currentTarget.dataset.type;

    // 处理按钮点击逻辑
    switch (type) {
      case 'classic1':
        this.handleClassic1Click();
        break;
      case 'classic2':
        this.handleClassic2Click();
        break;
      case 'classic3':
        this.handleClassic3Click();
        break;
      default:
        break;
    }
  },
  handleClassic1Click: function () {
    // 处理经典1按钮点击逻辑
    this.setData({
      buttonClicked: {
        classic1: true,
        classic2: false,
        classic3: false,
      },
    });

    // 设置定时器，1秒后重置按钮状态
    setTimeout(() => {
      this.setData({
        buttonClicked: {
          classic1: false,
          classic2: false,
          classic3: false,
        },
      });
    }, 1000);
  },
  handleClassic2Click: function () {
    // 处理经典2按钮点击逻辑
    this.setData({
      buttonClicked: {
        classic1: false,
        classic2: true,
        classic3: false,
      },
    });

    // 设置定时器，1秒后重置按钮状态
    setTimeout(() => {
      this.setData({
        buttonClicked: {
          classic1: false,
          classic2: false,
          classic3: false,
        },
      });
    }, 1000);
  },
  handleClassic3Click: function () {
    // 处理经典3按钮点击逻辑
    this.setData({
      buttonClicked: {
        classic1: false,
        classic2: false,
        classic3: true,
      },
    });

    // 设置定时器，1秒后重置按钮状态
    setTimeout(() => {
      this.setData({
        buttonClicked: {
          classic1: false,
          classic2: false,
          classic3: false,
        },
      });
    }, 1000);
  },
  buttonPress: function (event) {
    const buttonType = event.currentTarget.dataset.type;
    this.setData({
      pressingButton: buttonType,
    });
  },
  buttonRelease: function () {
    this.setData({
      pressingButton: null,
    });
  },
});
