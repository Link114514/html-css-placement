var number = new Array("133", "149", "153", "113", "177", "180", "181", "189", "191", "199");
var temp;

$(function() {
    for(var i = 0; i <10; i++) {
        temp = number[i];
        console.log(temp); //手机号前3位	
        for (var j = 0; j <8; j++) {
            temp = temp + Math.floor(Math.random() * 10); //随机取一个0~9的整数，循环8次
        }
        $("#div_p").append('<p>' + temp + '</p>');
        console.log(temp); //11位手机号
        $('#div_p p').eq(i).css('left', '-100%'); //初始化位置在左侧
    }

    var delay = 300; //动画间隔
    $('#div_p p').each(function(index) {
        $(this).delay(delay * index).animate({
            left: '45%'
        });
    });

    $("#btn_prize").on("click", function() {
        temp = Math.floor(Math.random() * 10);
        for(var i = 0; i < 10; i++) {
            if(i != temp) {
                $("#div_p p:eq(" + i + ")").fadeOut(); //淡出
            } else {
                $("#div_p p:eq(" + temp + ")").animate({
                    fontSize: '200%',
                    left: '44.6%',
                    backgroundColor: '#12aaa0',
                    color: '#fff',
                    width: '210px'
                });
            }
        }

        $("#btn_prize").attr('disabled', 'disabled'); //给按钮添加disabled属性
    });

    $("#btn_re").click(function() {
        $("body").toggle("puff");
        setTimeout(function() {
            window.location.reload();
        }, 1000);
    });
});
